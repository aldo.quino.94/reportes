const {Pool} = require('pg')
const pool = new Pool({
	host: 'localhost:5432',
	database: process.env.PGDATABASE,
	user: process.env.PGUSER,
	password: process.env.PGPASSWORD,
})

module.exports = {
	query: (text, params, callback) => {
		return pool.query(text, params, callback)
	},
	tx: async(callback) => {
		let res = null
		let err = ''
		let client = await pool.connect()
		try {
			await client.query('BEGIN')
			try {
				res = await callback(client)
				client.query('COMMIT')
			} catch (e) {
				console.error(e)
				err = ''+e
				client.query('ROLLBACK')
			}
		} finally {
			client.release()
			if (err) throw err
			return res
		}
	}
}
