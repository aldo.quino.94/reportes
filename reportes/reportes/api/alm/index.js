const api = require('express')()
api.use('/productos', require('./productos'))
api.use('/saldos', require('./saldos'))
module.exports = api