const express = require('express')
const db = require('../db')
const util = require('../util')
const { jsPDF } = require('jspdf')
require('jspdf-autotable')
const XLSX = require('xlsx')

api = express()
api.use(require('cors')({origin: true}))

//productos
api.get("/:filetype", async (req, res) => {
	console.log('/alm/productos', JSON.stringify(req.query))
	try {
		//obtener datos de la base de datos para el reporte
		let {rows: table} = await db.tx(async(client) => {
			return await client.query(`SELECT * FROM api.palm_productos($1)`, [
				req.query.id_presentacion,
			])
		})
		if(!table.length) return res.sendStatus(404)
		let titulo = "Reporte - Productos"
		if(req.params.filetype == 'xlsx') {
			//cabecera
			let header = []
			for (let h in table[0]) {
				header.push(h)
			}
			//cuerpo
			let body = []
			for (let row of table) {
				let r = []
				for (let h of header) {
					r.push(row[h] || '')
				}
				body.push(r)
			}
			//xlsx
			body.unshift(header.map(e => util.titleCase(e)))
			let wb = XLSX.utils.book_new()
			let ws = XLSX.utils.aoa_to_sheet(body)
			XLSX.utils.book_append_sheet(wb, ws, titulo)
			res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
			res.setHeader('Content-Disposition', `inline; filename=${titulo}.xlsx`)
			return res.send(new Buffer.from(XLSX.write(wb, { bookType: 'xlsx', type: 'buffer' })))
		} else if (req.params.filetype == 'pdf') {
			//armar el cuerpo del reporte
			let total = {
				saldo_inicial: 0,
			}
			let body = []
			for (let row of table) {
				body.push([
					row.presentacion,
					row.codigo,
					row.nombre,
					util.round(row.saldo_inicial),
				])
				total.saldo_inicial += +row.saldo_inicial
			}
			//totales
			body.push([
				{ content: 'TOTAL', colSpan: 3, styles: { halign: 'right', fillColor: [220, 220, 220] } },
				{ content: util.round(total.saldo_inicial), styles: { fillColor: [220, 220, 220] } },
			])
			//generar documento pdf
			let doc = new jsPDF('landscape', 'cm', 'a4')
			doc.addImage((await util.pdfImg('asset/logo.jpg')), 'JPEG', 2, 1, 4, 1, 'logo', 'FAST')
			doc.text("PRODUCTOS", doc.internal.pageSize.width / 2, 2, 'center')
			doc.autoTable({
				startY: 5,
				head: [[
					{ content: 'PRESENTACION', styles: { halign: 'left' } },
					{ content: 'CODIGO', styles: { halign: 'left' } },
					{ content: 'NOMBRE', styles: { halign: 'left' } },
					{ content: 'SALDO INICIAL', styles: { halign: 'right' } },
				]],
				columnStyles: {
					3: { halign: 'right' },
				},
				body: body,
			})
			let totalPages = doc.internal.getNumberOfPages()
			for (let p = 1; p <= totalPages; p++) {
				doc.setPage(p)
				doc.setFontSize(10)
				doc.text(`${p} / ${totalPages}`, doc.internal.pageSize.width - 2, doc.internal.pageSize.height - 1)
			}
			doc.setProperties({
				title: titulo
			})
			//devolver documento
			res.setHeader('Content-Type', 'application/pdf')
			res.setHeader('Content-Disposition', `inline; filename=${titulo}.pdf`)
			return res.send(new Buffer.from(doc.output('arraybuffer')))
		}
		return res.sendStatus(400)
	} catch (err) {
		console.error(err)
		return res.sendStatus(500)
	}
})
module.exports = api